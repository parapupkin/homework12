﻿<?php
    $connect = mysqli_connect("localhost", "root", "","First");  
    mysqli_set_charset($connect, "utf8");  
    $sql = "select * from books";    
    if (!empty($_POST['isbn']) || !empty($_POST['name']) || !empty($_POST['author'])) {        
        $sql = "select * from books where ISBN like '%{$_POST['isbn']}%' and name like '%{$_POST['name']}%' and author like '%{$_POST['author']}%'";
        $res = mysqli_query($connect, $sql);
    } else {
        $res = mysqli_query($connect, $sql);
      }   
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style type="text/css">
        table {
            border-collapse: collapse;            
        }
        td, th {
            border: 1px solid grey;
            padding: 5px;
        }
        th {
            background-color: #EEEEEE;
        }
        input {
            margin-bottom: 20px;
        }

    </style>
</head>
<body>
    <h1>Библиотека успешного человека</h1>
    <?php if (!$res->num_rows) {
          echo "<h2>По Вашему запросу не найдено совпадений</h2>";
          } else {
    ?>
    <form method="post">
        <input name="isbn" placeholder="ISBN" value="<?php if(!empty($_POST['isbn'])) echo($_POST['isbn'])?>">
        <input name="name" placeholder="Название книги" value="<?php if (!empty($_POST['name'])) echo($_POST['name'])?>">
        <input name="author" placeholder="Автор книги" value="<?php if (!empty($_POST['author'])) echo($_POST['author'])?>">
        <input type="submit" name="">
    </form>
    <table>
        <tr>
            <th>Название</th>
            <th>Автор</th>
            <th>Год выпуска</th>
            <th>Жанр</th>
            <th>ISBN</th>
        </tr>
        <?php while ($date = mysqli_fetch_array($res)) { ?>  
        <tr>              
            <td><?php echo $date['name'] ?></td>
            <td><?php echo $date['author']?></td>
            <td><?php echo $date['year']?></td>            
            <td><?php echo $date['genre']?></td>
            <td><?php echo $date['isbn']?></td>              
        </tr>
        <?php }} ?>
    </table>
    
</body>
</html>